# DEPTKit

[![CI Status](http://img.shields.io/travis/Joao Nascimento/DEPTKit.svg?style=flat)](https://travis-ci.org/Joao Nascimento/DEPTKit)
[![Version](https://img.shields.io/cocoapods/v/DEPTKit.svg?style=flat)](http://cocoapods.org/pods/DEPTKit)
[![License](https://img.shields.io/cocoapods/l/DEPTKit.svg?style=flat)](http://cocoapods.org/pods/DEPTKit)
[![Platform](https://img.shields.io/cocoapods/p/DEPTKit.svg?style=flat)](http://cocoapods.org/pods/DEPTKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DEPTKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "DEPTKit"
```

## Author

Joao Nascimento, joao.nascimento@tamtam.nl

## License

DEPTKit is available under the MIT license. See the LICENSE file for more info.
