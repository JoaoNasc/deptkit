//
//  Layer.swift
//  Pods
//
//  Created by Joao Nascimento on 22/09/16.
//
//

import UIKit

public extension CALayer {
    
    func setBorderColorFromUIColor(_ color: UIColor){
        borderColor = color.cgColor
    }
}
