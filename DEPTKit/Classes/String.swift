//
//  String.swift
//  Pods
//
//  Created by Joao Nascimento on 31/08/16.
//
//

import Foundation

public extension String{
    
    func empty() -> Bool {
        return self.characters.count == 0
    }
    
    func lenght() -> Int{
        return self.characters.count
    }
    
    func wordCount() -> Int{
        if self.empty(){
            return 0
        }
        return self.components(separatedBy: " ").count
    }
    
    func localizedString() -> String{
        return NSLocalizedString(self, comment: self)
    }
}
