//
//  Array.swift
//  Pods
//
//  Created by Joao Nascimento on 22/09/16.
//
//

import Foundation

public extension Array {
    subscript (safe index: UInt) -> Element? {
        return Int(index) < count ? self[Int(index)]: nil
    }
    
}

public extension Array where Element: Equatable {
 
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
    
    mutating func removeObject(_ object: Element) {
        if let index = self.index(of: object) , count > 0 && index > 0{
            self.remove(at: index)
        }
    }
}
