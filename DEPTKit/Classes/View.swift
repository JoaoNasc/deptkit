//
//  View.swift
//  Pods
//
//  Created by Joao Nascimento on 31/08/16.
//
//

import Foundation

// ***** VIEW SIZE *****

public extension UIView {
    
    var size : CGSize {
        get {
            return frame.size
        }
        set {
            frame = CGRect(x: left, y: top, width: newValue.width, height: newValue.height)
        }
    }
    
    var width : CGFloat {
        get {
            return frame.width
        }
        set {
            frame = CGRect(x: left, y: top, width: newValue, height: height)
        }
    }
    
    var height : CGFloat {
        get {
            return frame.height
        }
        set {
            frame = CGRect(x: left, y: top, width: width, height: newValue)
        }
    }
}


// ***** VIEW POSITION *****

public extension UIView{
    
    var left : CGFloat {
        get {
            return frame.origin.x
        }
        set {
            frame.origin.x = newValue
        }
    }
    
    var top : CGFloat {
        get {
            return frame.origin.y
        }
        set {
            frame.origin.y = newValue
        }
    }
    
    var right : CGFloat {
        get {
            return left + width
        }
        set {
            left = newValue - width
        }
    }
    
    var bottom : CGFloat {
        get {
            return top + height
        }
        set {
            top = newValue - height
        }
    }
    
    var centerX : CGFloat {
        get {
            return left + width/2
        }
        set {
            left = newValue - width/2
        }
    }
    
    var centerY : CGFloat {
        get {
            return top + height/2
        }
        set {
            top = newValue - height/2
        }
    }
}


// ***** VIEW SIZE *****
    
public extension UIView{
    func sameSizeAsView(_ view: UIView) {
        self.size = view.size
    }
    
    func halfSizeOfView(_ view: UIView) {
        self.width = view.width/2
        self.height = view.height/2
    }
    
    func halfWidthOfView(_ view: UIView) {
        self.width = view.width/2
        self.height = view.height
    }
    
    func halfHeightOfView(_ view: UIView) {
        self.width = view.width
        self.height = view.height/2
    }
    
    
}


// ***** VIEW ALIGNEMENT *****

public extension UIView{
   
    func centerIn(_ view: UIView) {
        self.centerX = view.width/2
        self.centerY = view.height/2
    }
    
    func centerAlignWith(_ view: UIView) {
        self.center = view.center
    }
    
    func centerAlignXIn(_ view: UIView) {
        self.centerX = view.width/2
    }
    
    func centerAlignYIn(_ view: UIView) {
        self.centerY = view.height/2
    }
    
    func centerYLeftAlignIn(_ view: UIView) {
        self.centerY = view.height/2
        self.left = 0
    }
    
    func centerYRightAlignIn(_ view: UIView) {
        self.centerY = view.height/2
        self.right = view.width
    }
    
    func centerXTopAlignIn(_ view: UIView) {
        self.centerX = view.width/2
        self.top = 0
    }
    
    func centerXBottomAlignIn(_ view: UIView) {
        self.centerX = view.width/2
        self.bottom = view.height
    }
}
