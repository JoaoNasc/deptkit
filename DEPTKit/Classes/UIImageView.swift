//
//  UIImageView.swift
//  Pods
//
//  Created by Joao Nascimento on 22/09/16.
//
//

import UIKit

public extension UIImageView{
    
    func animatedImageViewWithFirstImageName(_ imageBaseName:String, numberOfImages:Int, duration:TimeInterval){
        var images = [UIImage]()
        for index in 0...numberOfImages{
            let image = UIImage(named:String(format:"%@%02d",imageBaseName, index))
            images.append(image!)
        }
        self.animationImages = images
        self.animationDuration = duration
        self.animationRepeatCount = 1
    }
    
    func reverseAnimatedImageViewWithFirstImageName(_ imageBaseName:String, numberOfImages:Int, duration:TimeInterval){
        var images = [UIImage]()
        for index in (0...numberOfImages).reversed(){
            let image = UIImage(named:String(format:"%@%02d",imageBaseName, index))
            images.append(image!)
        }
        self.animationImages = images
        self.animationDuration = duration
        self.animationRepeatCount = 1
    }
}
