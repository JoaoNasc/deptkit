//
//  UIImage.swift
//  Pods
//
//  Created by Joao Nascimento on 22/09/16.
//
//


import UIKit

public extension UIImage{
    
    func imageWithColor(_ color: UIColor, size: CGSize) -> UIImage? {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        guard let image : UIImage = UIGraphicsGetImageFromCurrentImageContext() else{
            return nil
        }
        UIGraphicsEndImageContext()
        return image
    }
}
