//
//  Double.swift
//  Pods
//
//  Created by Joao Nascimento on 22/09/16.
//
//

import Foundation

public extension Double {
    
    public static func random(_ lower: Double, upper: Double) -> Double {
        let r = Double(arc4random()) / Double(UInt64.max)
        return (r * (upper - lower)) + lower
    }
}
