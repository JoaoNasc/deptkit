//
//  UIColor.swift
//  Pods
//
//  Created by Joao Nascimento on 30/09/16.
//
//

import UIKit

public extension UIColor{
    
    class func colorWith(R: CGFloat, G: CGFloat, B: CGFloat, alpha: CGFloat = 1.0) -> UIColor{
        return UIColor.init(red: R/255, green: G/255, blue: B/255, alpha: alpha)
    }
    
    class func grayColorWith(RGB: CGFloat, alpha: CGFloat = 1.0) -> UIColor{
        return UIColor.init(red: RGB/255, green: RGB/255, blue: RGB/255, alpha: alpha)
    }
    
    class func colorWith(hex: String) -> UIColor{
        var cString = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = cString.replacingOccurrences(of: "#", with: "")
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
