//
//  ViewController.swift
//  DEPTKit
//
//  Created by Joao Nascimento on 09/21/2016.
//  Copyright (c) 2016 Joao Nascimento. All rights reserved.
//

import UIKit
import DEPTKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        makeLemonade()
    }
    
    func makeLemonade(){
        let aView = UIView()
        aView.backgroundColor = UIColor.red
        aView.size = CGSize(width: 100, height: 100)
        
        let anotherView = UIView()
        anotherView.backgroundColor = UIColor.yellow.withAlphaComponent(0.5)
        anotherView.sameSizeAsView(aView)
        anotherView.width += 50
        anotherView.height += 50
        
        let yetAnotherView = UIView()
        yetAnotherView.backgroundColor = UIColor.grayColorWith(RGB: 200, alpha: 0.5)
        yetAnotherView.halfHeightOfView(anotherView)
        
        let label1 = UILabel()
        label1.backgroundColor = UIColor.colorWith(hex: "119A39")
        label1.halfHeightOfView(aView)
        label1.textAlignment = .center
        label1.numberOfLines = 2
        label1.minimumScaleFactor = 0.2
        label1.text = "Words in this sentence:"
        
        let label2 = UILabel()
        label2.backgroundColor = UIColor.red
        label2.sameSizeAsView(label1)
        label2.textAlignment = .center
        if let label1Text = label1.text{
            label2.text = "\(label1Text.wordCount())"
        }

        aView.centerIn(view)
        anotherView.centerAlignWith(aView)
        yetAnotherView.centerAlignXIn(view)
        yetAnotherView.bottom = anotherView.top
        label1.centerXTopAlignIn(aView)
        label2.centerXBottomAlignIn(aView)
        
        view.addSubview(aView)
        view.addSubview(anotherView)
        view.addSubview(yetAnotherView)
        aView.addSubview(label1)
        aView.addSubview(label2)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

